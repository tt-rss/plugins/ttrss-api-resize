## Resizes images as requested by API clients

Downsamples images if an API client requests the backend to do so. Useful for
slow or unreliable network connections, i.e. mobile internet.

May also be enabled through the web UI (Preferences -> Feeds).

This plugin handles an additional parameter passed to ``getHeadlines`` 
or ``getArticle`` API calls:

* ``resize_width`` (int) - target width (or height, plugin resizes based on a larger dimension)

Images are saved in a WEBP format.

## Installation

Plugin assumes your tt-rss image cache (``cache/images``) is writable 
and GD library is available to PHP.

1. git clone to ``plugins.local/api_resize_media`` and enable the plugin (preferences - plugins)
2. Enable "downsample images" option in the API client (for example, tt-rss-android)

## License

GPLv3
